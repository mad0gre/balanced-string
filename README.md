# balanced-string
Balance a string by removing extra parentheses.


## Usage
From the project folder:

##### Linux
`./balanced.py "inputtext"`

##### Linux or Windows
`python balanced.py "inputtext"`


### Optional Arguments
- `-h`, `--help`: Display help message.


## Requirements
Python 3 is installed on the environment.