#! /usr/bin/env python

from argparse import ArgumentParser


def balance(unbalanced):
    p_stack = []
    chars = list(unbalanced)

    for i in range(len(chars)):
        if chars[i] == ')':
            if len(p_stack) == 0:
                chars[i] = ''  # Remove unbalanced closing parenthesis.
            else:
                p_stack.pop()
        if chars[i] == '(':
            p_stack.append(i)

    for i in p_stack:
        chars[i] = ''  # Removed all unclosed parenthesis

    return ''.join(chars)


def create_arg_parser():
    parser = ArgumentParser(description="Makes string balanced by removing the least amount of extra parentheses")
    parser.add_argument('input', help="input string to be balanced")
    return parser


def main():
    parser = create_arg_parser()
    args = parser.parse_args()
    output = balance(args.input)
    print(output)


if __name__ == '__main__':
    main()
